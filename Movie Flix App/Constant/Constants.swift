//
//  Constants.swift
//
//
//
//
//

import UIKit

class Constants: NSObject {
    /*
     * Constants regarding to API Calling
     */
    static let baseURL = "https://api.themoviedb.org/3/movie/"
    static let NOW_PLAYING = "now_playing?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
    static let TOP_RATED = "top_rated?api_key=a07e22bc18f5cb106bfe4cc1f83ad8ed"
}
