//
//  NowPlayingVideoViewController.swift
//  Movie Flix App
//
//  
//
//

import UIKit
import Alamofire
class NowPlayingVideoViewController: BaseViewController {

    @IBOutlet weak var nowPlayingCollectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var nodataLabel: UILabel!
    let inset: CGFloat = 10
       let minimumLineSpacing: CGFloat = 10
       let minimumInteritemSpacing: CGFloat = 10
       let cellsPerRow = 2
    var moviesModelArray = [MoviesModel]()
       var currentMoviesModelArray = [MoviesModel]()
    override func viewDidLoad() {
        setUp()
        super.viewDidLoad()
 self.nowPlayingCollectionView.register(UINib(nibName: "MoviesCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "MoviesCollectionViewCell")
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        getNowPlayingMovies()
        searchBar.searchTextField.clearButtonMode = .never
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension NowPlayingVideoViewController {
    func getNowPlayingMovies() {
        let urlResponce = String(format: "%@%@",Constants.baseURL,Constants.NOW_PLAYING)
        self.startLoading()
        AF.request( urlResponce,method: .get ,parameters: [:])
            .responseJSON { response in
                    switch response.result {
                   case .success(let value):
                   self.stopLoading()
                     print("movies_response",response)
                        if let movieJSON = value as? [String: Any] {
                            let movieData = movieJSON["results"] as? [[String: AnyObject]]
                            // print("movieJSON",movieData)
                            for i in movieData!{
                                let movieNames = i["title"] as! String
                                let movieimg = i["poster_path"] as! String
                                let movieDes = i["overview"] as! String
                                let movieDate = i["release_date"] as! String
                                let movieAvg = i["vote_average"] as! Double
                                let moviePopularity = i["popularity"] as! Double
                                self.moviesModelArray.append(MoviesModel(title1: movieNames, overview1: movieDes, poster_path1: movieimg, release_date1: movieDate, vote_average1: movieAvg, popularity1: moviePopularity))
                                self.currentMoviesModelArray = self.moviesModelArray
                            }
                              self.nowPlayingCollectionView.reloadData()
                        }
                    case .failure(let error):
                        print(error)
                        let alert = UIAlertController(title: "Oops!", message: "No internet connection", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                        DispatchQueue.main.async {
                            self.present(alert, animated: true, completion: nil)
                        }
                    }
                  
            }
    }
    func setUp() {
     let itemSize = UIScreen.main.bounds.width/2 - 3
     let layout = UICollectionViewFlowLayout()
     layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
     layout.itemSize = CGSize(width: itemSize, height: itemSize)
     layout.minimumInteritemSpacing = 3
     layout.minimumLineSpacing = 3
     nowPlayingCollectionView.collectionViewLayout = layout
    }
}
extension NowPlayingVideoViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate{
     func numberOfSections(in collectionView: UICollectionView) -> Int {
         return 1
     }
     func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return currentMoviesModelArray.count
     }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         if let cell: MoviesCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MoviesCollectionViewCell", for: indexPath) as? MoviesCollectionViewCell {
            cell.layer.borderColor = UIColor.lightGray.cgColor
            cell.layer.borderWidth = 1.0
            cell.moreButton.tag = indexPath.row
            cell.moreButton.addTarget(self, action: #selector(moreButtonClick), for: .touchUpInside)
             let data = currentMoviesModelArray[indexPath.row]
                cell.movieNameLabel.text = data.title
                cell.movieDescriptionLabel.text = data.overview
                 cell.movieDescriptionLabel.sizeToFit()
                let imageURl = data.poster_path
                let posterUrl = "https://image.tmdb.org/t/p/w342" + imageURl
                // print("posterUrl",posterUrl)
                 let url = URL(string:posterUrl)
                 if let data = try? Data(contentsOf: url!){
                     cell.movieImage.image = UIImage(data: data)!
            }
             return cell
         }
         return UICollectionViewCell()
     }
    @objc func moreButtonClick(_ sender: UIButton) {
       let actionSheetController = UIAlertController(title: "Please select", message: "", preferredStyle: .actionSheet)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            print("Cancel")
        }
        actionSheetController.addAction(cancelActionButton)

        let deleteActionButton = UIAlertAction(title: "Delete", style: .default) { action -> Void in
            print("Delete")
            let alert = UIAlertController(title: "Movie Flix",message: "Are you sure you want to delete?", preferredStyle: .alert)
            let addEvent = UIAlertAction(title: "Yes", style: .default) { (_) -> Void in
                print("Deleted")
            }
            let cancleEvent = UIAlertAction(title: "Cancel", style: .default) { (_) -> Void in
                print("No")
            }
            
            alert.addAction(addEvent)
            alert.addAction(cancleEvent)
            self.present(alert, animated: true, completion:  nil)
            
        }
        actionSheetController.addAction(deleteActionButton)
        self.present(actionSheetController, animated: true, completion: nil)
    }
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = currentMoviesModelArray[indexPath.row]
       let url =  data.poster_path
        let title =  data.title
        let des = data.overview
        let release_date =  data.release_date
        let vote_average = data.vote_average
        let popularity =  data.popularity
        print("clickTable")
        let vc = storyboard?.instantiateViewController(identifier: "MovieDetailViewController") as! MovieDetailViewController
        vc.imagUrl = url
        vc.movieName = title
        vc.movieDate = release_date
        vc.movieTime = popularity
        vc.movieRating = vote_average
        vc.movieDescription = des
        navigationController?.pushViewController(vc, animated: true)
 
     }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let edge    : CGFloat = 10.0
        let spacing : CGFloat = 10.0
        let noOfColumn = 2
        let collectionviewWidth = collectionView.frame.width
        let bothEdge =  CGFloat(edge + edge) // left + right
        let excludingEdge = collectionviewWidth - bothEdge
        let cellWidthExcludingSpaces = excludingEdge - (CGFloat(noOfColumn - 1) * spacing)
        let finalCellWidth = cellWidthExcludingSpaces / CGFloat(noOfColumn)
                 return CGSize(width: finalCellWidth, height: 350)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
          return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
          return minimumLineSpacing
      }
      
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
          return minimumInteritemSpacing
      }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String){
        guard !searchText.isEmpty else {
            currentMoviesModelArray = moviesModelArray
            nowPlayingCollectionView.reloadData()
            return
            
        }
        currentMoviesModelArray = moviesModelArray.filter({ movie -> Bool in
            //guard let text = searchBar.text else {return false}
            return movie.title.contains(searchText)
        })
        if currentMoviesModelArray.count <= 0 {
            nodataLabel.isHidden = false
            nowPlayingCollectionView.isHidden = true
        }else {
            nodataLabel.isHidden = true
            nowPlayingCollectionView.isHidden = false
        }
        self.nowPlayingCollectionView.reloadData()
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        self.searchBar.endEditing(true)
        guard !searchBar.text!.isEmpty else {
                   currentMoviesModelArray = moviesModelArray
                   nowPlayingCollectionView.reloadData()
                   return
        }
        nowPlayingCollectionView.reloadData()
    }
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.showsCancelButton = true
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.text = ""
        searchBar.showsCancelButton = false
        guard !searchBar.text!.isEmpty else {
            currentMoviesModelArray = moviesModelArray
            nowPlayingCollectionView.reloadData()
            return
        }
        if currentMoviesModelArray.count <= 0 {
            nodataLabel.isHidden = false
            nowPlayingCollectionView.isHidden = true
        }else {
            nodataLabel.isHidden = true
            nowPlayingCollectionView.isHidden = false
        }
        nowPlayingCollectionView.reloadData()
    }
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
         self.searchBar.endEditing(true)
    }
}

