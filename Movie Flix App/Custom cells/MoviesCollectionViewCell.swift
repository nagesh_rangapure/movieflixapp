//
//  MoviesCollectionViewCell.swift
//  Movie Flix App
//
//  Created by Mac on 21/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class MoviesCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieDescriptionLabel: UILabel!
    @IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        movieDescriptionLabel.sizeToFit()
        // Initialization code
    }

}
