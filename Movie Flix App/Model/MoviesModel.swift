//
//  DeviceModels.swift
//  ProjectAlex
//
//  Created by SCISPLMAC003 on 14/04/18.
//  Copyright © 2018 Ravi Deshmukh. All rights reserved.
//

import UIKit

class MoviesModel: NSObject {
    var title : String
    var overview : String
    var poster_path : String
    var release_date : String
    var vote_average : Double
    var popularity: Double
      init(title1:String,overview1:String,poster_path1:String,release_date1:String,vote_average1:Double,popularity1:Double) {
          self.title = title1
          self.overview = overview1
          self.poster_path = poster_path1
          self.release_date = release_date1
          self.vote_average = vote_average1
          self.popularity = popularity1
      }
}
