//
//  MovieDetailViewController.swift
//  Movie Flix App
//
//  Created by Mac on 20/05/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {
@IBOutlet weak var movieImage: UIImageView!
    @IBOutlet weak var movieNameLabel: UILabel!
    @IBOutlet weak var movieDescriptionTextView: UITextView!
    @IBOutlet weak var movieDateLabel: UILabel!
    @IBOutlet weak var movieRatingLabel: UILabel!
    @IBOutlet weak var movieTimeLabel: UILabel!
    var imagUrl = ""
    var movieName = ""
    var movieDescription = ""
    var movieDate = ""
    var movieRating = 0.0
    var movieTime = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = movieName
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("imagUrl",imagUrl)
        movieDescriptionTextView.text = movieDescription
        movieNameLabel.text = movieName
        movieRatingLabel.text = "\(movieRating)"
        movieDateLabel.text = movieDate
        movieTimeLabel.text = "\(movieTime)"
        let url = URL(string: "https://image.tmdb.org/t/p/original" + imagUrl)
        if let data = try? Data(contentsOf: url!){
            movieImage.image = UIImage(data: data)!
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
